import { calcular } from './calculadora';

import { OPERACION_SUMA,
  OPERACION_RESTA,
  OPERACION_MULTIPLICACION,
  OPERACION_DIVICION} from './constants';

describe('calculadora.test', () => {
  it('Probar la suma', () => {
    expect(calcular(1, 2, OPERACION_SUMA)).toBe(3);
  });

  it('Probar la resta',() => {
    expect(calcular(4,2,OPERACION_RESTA)).toBe(2);
    });

  it('Probar la multiplicacion',() => {
      expect(calcular(13,2,OPERACION_MULTIPLICACION)).toBe(26);
    });

  it('Probar la divicion',() => {
      expect(calcular(10,2,OPERACION_DIVICION)).toBe(5);
    });

  it('probar que te mande un error si le mandas a la función valores no númericos', () => {
    expect(()=>{calcular('1','2',OPERACION_SUMA)}).toThrowError('Solo se admiten numeros');
  });
});
