import { OPERACION_SUMA,
         OPERACION_RESTA,
         OPERACION_MULTIPLICACION,
         OPERACION_DIVICION} from './constants';
import { type } from 'os';
/**
 *
 * @param numero1 - Primero número a sumar
 * @param numero2 - Segundo número a sumar
 * @param operacion - sumar, restar, dividir, multiplicar
 */
function calcular(numero1: any, numero2: any, operacion: string):any {
  if (typeof numero1 !== 'number' || 
      typeof numero2 !== 'number') {
    throw new Error('Solo se admiten numeros');
  }
  let resultado: number;
  if (operacion === OPERACION_SUMA) {
    resultado = numero1 + numero2;
  }
  if (operacion === OPERACION_RESTA) {
    resultado = numero1 - numero2;
  }
  if (operacion === OPERACION_MULTIPLICACION) {
    resultado = numero1 * numero2;
  }
  if (operacion === OPERACION_DIVICION) {
    resultado = numero1 / numero2;
  }
  return resultado;
}

export {
  calcular,
};
